package com.pankaj.brill.hidingview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView listView;
    ArrayList<String> arrayList=new ArrayList<>();
    Button button;
    boolean isShowing=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView=(RecyclerView) findViewById(R.id.myList);
        button=(Button)findViewById(R.id.show);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isShowing){
                    listView.setVisibility(View.VISIBLE);
                    isShowing=true;
                }else{
                    listView.setVisibility(View.GONE);
                    isShowing=false;
                }

            }
        });
        arrayList.add("one");
        arrayList.add("two");
        arrayList.add("one");
        arrayList.add("two");
        arrayList.add("one");
        arrayList.add("two");
        arrayList.add("one");
        arrayList.add("two");
        arrayList.add("one");
        arrayList.add("two");
        arrayList.add("one");
        arrayList.add("two");
        arrayList.add("one");
        arrayList.add("two");
        arrayList.add("one");
        arrayList.add("two");
        arrayList.add("one");
        arrayList.add("two");
        arrayList.add("one");
        arrayList.add("two");
        arrayList.add("one");
        arrayList.add("two");
        arrayList.add("one");
        arrayList.add("two");

       RecycleAdapter recycleAdapter=new RecycleAdapter(arrayList);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(listView.getContext());
        listView.setHasFixedSize(true);
        listView.setLayoutManager(layoutManager);
        listView.setAdapter(recycleAdapter);
    }
}
