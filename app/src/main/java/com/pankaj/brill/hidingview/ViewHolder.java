package com.pankaj.brill.hidingview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by root on 6/1/17.
 */

public class ViewHolder extends RecyclerView.ViewHolder {
   public TextView textView;
    View view;
    public ViewHolder(View itemView) {
        super(itemView);
        this.view=itemView;
        textView=(TextView)view.findViewById(R.id.list_text);
    }
}
